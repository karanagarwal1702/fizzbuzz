const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const cors = require('cors')
const express = require('express')
const morgan = require('morgan')

const routes = require('./routes')

// Initialising App
var app = express()

// adding the configurations to make the api publically accessible
let corsOptions = {
	origin: true,
	credentials: true,
	allowedHeaders: 'Content-Type, Authorization, Content-Length, X-Requested-With, Authentication'
}

app.use(cors(corsOptions))
app.options('*', cors(corsOptions))

app.listen(3000)

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())

app.use(morgan('dev'))

app.use('/', routes)

// Error handlers
// development error handler; will print stacktrace
if (process.env.environment === 'development') {
	app.use(function (err, req, res, next) {
		res.status(err.status || 500).json({ message: err.message, error: err })
		console.log(err)
	})
} else {
	// production error handler; no stacktraces leaked to user
	app.use(function (err, req, res, next) {
		res.status(err.status || 500).json({ message: 'Something went wrong!' })
		console.log(err)
	})
}

module.exports = app
