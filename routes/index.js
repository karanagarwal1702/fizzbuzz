const express = require('express')

const fizzbuzz = require('./fizzbuzz')

const router = express.Router()

router.use('/fizzbuzz', fizzbuzz)

module.exports = router
