const express = require('express')

const router = express.Router()

router.get('/', function (req, res, next) {
	if (!req.query.count) {
		return res.status(400).json({ error: 'Expected a query parameter named count' })
	}
	let count = parseInt(req.query.count)
	if (isNaN(count)) {
		return res.status(400).json({ error: 'Expected an integer as count in query parameters' })
	}
	let result = []
	for (let i = 1; i <= count; i++) {
		let item
		if (i % 3 === 0) {
			if (i % 5 === 0) {
				item = 'FizzBuzz'
			} else {
				item = 'Fizz'
			}
		} else if (i % 5 === 0) {
			item = 'Buzz'
		} else {
			item = i.toString()
		}
		result.push(item)
	}
	res.status(200).json({ result })
})

module.exports = router
